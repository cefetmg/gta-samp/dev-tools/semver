FROM debian:10.7

RUN apt-get update && apt-get install -y npm git wget
RUN npm i -g npm semver
RUN wget https://github.com/git-chglog/git-chglog/releases/download/v0.15.0/git-chglog_0.15.0_linux_amd64.tar.gz \
    && mkdir -p /usr/local/git-chglog \
    && tar -C /usr/local/git-chglog -xzf git-chglog_0.15.0_linux_amd64.tar.gz \
    && ls -la /usr/local/git-chglog
ENV PATH="/usr/local/git-chglog:${PATH}"

ADD define_version /bin
ADD define_channel /bin
ADD should_promote /bin
ADD create_changelog /bin
ADD chglog/ chglog/
RUN chmod +x /bin/define_version
RUN chmod +x /bin/should_promote
RUN chmod +x /bin/create_changelog
RUN chmod +x /bin/define_channel

ENTRYPOINT bash